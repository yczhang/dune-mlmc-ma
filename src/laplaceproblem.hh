#ifndef DUNE_MLMC_LAPLACEPROBLEM_HH
#define DUNE_MLMC_LAPLACEPROBLEM_HH

#include "deterministicproblem.hh"
#include "functions.hh"

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>

#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/functionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/generalizedlaplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/l2functionalassembler.hh>
#include <dune/fufem/assemblers/transferoperatorassembler.hh>
#include <dune/fufem/assemblers/preconditioneddefectassembler.hh>

#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/refinedp1nodalbasis.hh>
#include <dune/fufem/functionspacebases/extensionbasis.hh>
#include <dune/fufem/functionspacebases/conformingbasis.hh>

#include <dune/solvers/iterationsteps/blockgsstep.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/transferoperators/compressedmultigridtransfer.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>

#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/estimators/fractionalmarking.hh>

#include <dune/solvers/common/boxconstraint.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/fufem/sharedpointermap.hh>



#include <dune/fufem/functions/vtkbasisgridfunction.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>



template <class GridType, class VectorType, class DomainType, class RangeType>
class LaplaceProblem : public DeterministicProblem<VectorType>
{
public:

    static const int dim = GridType::dimension;
    static const int nComponents = RangeType::dimension;

    typedef typename DomainType::field_type field_type;

    typedef Dune::BCRSMatrix<Dune::FieldMatrix<field_type, nComponents, nComponents> > MatrixType;

    typedef Dune::VirtualFunction<DomainType, RangeType> Function;
    typedef SharedPointerMap<std::string, Function > FunctionMap;

    typedef typename GridType::LeafGridView LeafView;

    typedef P1NodalBasis<LeafView,field_type> BasisType;

    LaplaceProblem(GridType& grid, Dune::ParameterTree& parset, const FunctionMap& functions) :
        grid_(grid),
        basis_(grid.leafGridView()),
        parset_(parset),
        functions_(functions)
    {
        x_.resize(basis_.size());
        x_ = 0.0;
    }
    void assemble();
    void solve();

    VectorType getSol()
    {
        //Functions::interpolate(nonConfBasis_, x_nc, Functions::makeFunction(basis_,x_));//
        //return x_nc;
    	return x_;
    };

    BasisType getBasis()
    {
        return basis_;
    }


private:
    GridType& grid_;
    MatrixType laplaceMatrix_;
    VectorType rhs_;
    VectorType x_;
    const FunctionMap& functions_;
    BasisType basis_;

    Dune::ParameterTree& parset_;
};

#include "laplaceproblem.cc"

#endif

