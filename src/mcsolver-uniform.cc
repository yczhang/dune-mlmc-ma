template <class GridType, class VectorType, class SampleType,class DomainType, class RangeType>
void MCSolver<GridType, VectorType, SampleType, DomainType, RangeType>::MLMC_solve_uniform()
{
    // initialize random number generator
    init_noise(time(NULL));

    const int NLevels = parset_.get<int>("NLevels");
    const int minRefine = parset_.get<int>("minRefine");
    std::string stochDistr_ = parset_.get<std::string>("stoch"); // get type of stoch.
    const int OPT_SN = parset_.get<int>("OPT_SN");

    
    // Uniformly refine the grid
    for (int i=0; i<minRefine; i++)// initial refinement of the Grid to "minRefine" Level.
        grid_.globalRefine(1);
    
    //initial the final solution.
    solution_.resize(grid_.leafGridView().size(dim));
    solution_ = 0.0;

    // define number of samples
    const double factor = parset_.get<double>("factorSN");
    const double epsilon = parset_.get<double>("epsilon");
    
   
    //MLMC Loop
    for (int level = 0; level < NLevels; level++)
    {
        std::cout<<"Level N° "<<level<<":"<<std::endl;
        
    //*****************determine the size of the samples***********************
        int NSamples;
        if (OPT_SN)
        {
            int l = level+1;
            NSamples = int(factor*pow(l, 2*(epsilon+1))*pow(2,2*(NLevels-l)));
        }
        else NSamples = parset_.get<int>("NSamples");
    //*************************************************************************       
   		grid_.globalRefine(1);		
	    grid_.postAdapt();
		
	    int lowerMaxLevel=grid_.maxLevel();
	    		
        lowerSubGrid_.createBegin();
        lowerSubGrid_.insertLevel(lowerMaxLevel-1);
        lowerSubGrid_.createEnd();
        
        VectorType lowerSol(lowerSubGrid_.leafGridView().size(dim));
		lowerSol=0.0; 
        
        higherSubGrid_.createBegin();
        higherSubGrid_.insertLevel(lowerMaxLevel);
        higherSubGrid_.createEnd();
        
        VectorType higherSol(higherSubGrid_.leafGridView().size(dim));
		higherSol=0.0;
   //***************************************************************************
		
        for (int i = 0; i < NSamples; i++)
        {
        	
            std::cout<<"Sample N° "<<i<<":"<<std::endl;

            // Filling up the samples
            SampleType sample(0);
            for (int j = 0; j < stochDim_; j++)
            {
                if ( stochDistr_ == "uniform")
                     sample[j] = 2. * jvrand_real2() - 1.;
                else if ( stochDistr_ == "normal")
                     sample[j] = jvrand_standard_normal();
                else if ( stochDistr_ == "gamma1")
                     sample[j] = - log(jvrand_real2()) - log(jvrand_real2()) - 2.;
                else if ( stochDistr_ == "gamma4")
                     sample[j] = - log(jvrand_real2()) - log(jvrand_real2()) - log(jvrand_real2()) - log(jvrand_real2()) - log(jvrand_real2()) - 5.;
                else
                     DUNE_THROW(Dune::NotImplemented, "Unknown stochastic distribution during MC sampling!");

            }

            //evaluate functions
            functions_.set("coefficient", new EvaluatedStochasticFunction<DomainType, SampleType, RangeType>(stochFunctions_["coefficient"],sample));
            functions_.set("rhs", new EvaluatedStochasticFunction<DomainType, SampleType, RangeType>(stochFunctions_["rhs"],sample));
            functions_.set("dirichletNodes", new EvaluatedStochasticFunction<DomainType, SampleType, RangeType>(stochFunctions_["dirichletNodes"],sample));
           
        //*************Coaser Grid*******************
           
            ProblemType lowerProblem(lowerSubGrid_, parset_, functions_); 
            lowerProblem.solve();
            lowerSol+= lowerProblem.getSol(); 
                     
        //*************Finer Grid********************    
            
            ProblemType higherProblem(higherSubGrid_, parset_, functions_);
            higherProblem.solve();
            higherSol+= higherProblem.getSol();

        }// samples end
        if (level==0)
        	lowerSol=0.0;
        		
      //**********Interpolate coaser solution to finer grid and get the difference***************
        
                
        LocalBasisType lowerBasis(lowerSubGrid_.leafGridView()); 
        GlobalBasisType higherBasis(grid_.leafGridView());
        //LocalBasisType higherBasis(higherSubGrid_.leafGridView()); 
                
        VectorType lowerToHigherSol;      
        Functions::interpolate(higherBasis, lowerToHigherSol, Functions::makeFunction(lowerBasis, lowerSol)); 
        higherSol-=lowerToHigherSol;
                
     //***********************************Adapte the difference to basis grid**************  
        minSubGrid_.createBegin();
        minSubGrid_.insertLevel(minRefine);
        minSubGrid_.createEnd();
        LocalBasisType minBasis(minSubGrid_.leafGridView()); 
        
        VectorType solToMinBasis;
        Functions::interpolate(minBasis, solToMinBasis, Functions::makeFunction(higherBasis, higherSol));
     
        solToMinBasis /= NSamples;
        
        solution_ += solToMinBasis;
    }
}
