template <class GridType, class VectorType, class DomainType, class RangeType>

void LaplaceProblem<GridType, VectorType, DomainType, RangeType>::assemble()
{

    // Global Assembler that calls the local assembler on each grid element and distributes the corresponding values onto the global indices
    OperatorAssembler<BasisType,BasisType> assembler(basis_, basis_);

    const Function& coeffFunction = functions_.get("coefficient");

    // Local Assembler
    typedef typename BasisType::LocalFiniteElement FE;
    GeneralizedLaplaceAssembler<GridType, FE, FE, Function> localLaplaceAssembler(coeffFunction, 2);

    // Compute the global problem matrix
    assembler.assemble(localLaplaceAssembler, laplaceMatrix_);
    //Assemble RHS
    /*
    for (int row=0; row< laplaceMatrix_.N() ; row++)
    {
    	for (int col=0; col< laplaceMatrix_.M() ; col++)
    	{
    		if (laplaceMatrix_.exists(row,col))
    				std::cout<<"("<<row<<","<<col<<")"<<laplaceMatrix_[row][col]<<std::endl;
    	}
    }
    */	

    // Global Assembler for functionals
    FunctionalAssembler<BasisType> globalFunctionalAssembler(basis_);

    const Function& rhsFunction = functions_.get("rhs");
    // Local Assembler
    L2FunctionalAssembler<GridType, FE> l2Assembler(rhsFunction,2);

    globalFunctionalAssembler.assemble(l2Assembler, rhs_);

}

template <class GridType, class VectorType, class DomainType, class RangeType>
void LaplaceProblem<GridType, VectorType, DomainType, RangeType>::solve()
{
    // The maximal number of multigrid iterations to be performed
    const int numIt            = parset_.get<int>("numIt");
    // The error tolerance until which shall be solved
    const double tolerance     = parset_.get<double>("tolerance");

    BoundaryPatch<LeafView> boundary(grid_.leafGridView(),true);
    Dune::BitSetVector<1> dirichletNodes;
	boundary.getVertices(dirichletNodes);

    // compute entries of solution vector for dirichlet nodes
    // as interpolation of given dirichlet function
    const Function& dirFunction = functions_.get("dirichletNodes");
    Functions::interpolate(basis_, x_, dirFunction, dirichletNodes);

    assemble();


    BlockGSStep<MatrixType, VectorType> baseSolverStep;
    EnergyNorm<MatrixType,VectorType> baseEnergyNorm(baseSolverStep);
    LoopSolver<VectorType> baseSolver(&baseSolverStep,
                                               100,
                                               1e-10,
                                               &baseEnergyNorm,
                                               Solver::QUIET);
    
    BlockGSStep<MatrixType, VectorType> presmoother;
    BlockGSStep<MatrixType, VectorType> postsmoother;

    MultigridStep<MatrixType, VectorType> multigridStep(laplaceMatrix_, x_, rhs_);

    multigridStep.setMGType(1, 2, 2);
    multigridStep.ignoreNodes_    = &dirichletNodes;
    multigridStep.basesolver_     = &baseSolver;
    multigridStep.setSmoother(&presmoother,&postsmoother);

    std::vector<CompressedMultigridTransfer<VectorType>* > mgTransfers(grid_.maxLevel());
    for (int i=0; i<mgTransfers.size(); i++) {
        mgTransfers[i] = new CompressedMultigridTransfer<VectorType>;
        mgTransfers[i]->setup(grid_, i, i+1);
    }

    multigridStep.setTransferOperators(mgTransfers);
    EnergyNorm<MatrixType, VectorType> energyNorm(multigridStep);

    LoopSolver<VectorType> solver(&multigridStep,
                                  numIt,
                                  tolerance,
                                  &energyNorm,
                                  Solver::FULL);
    
    solver.preprocess();
    // Compute solution
    solver.solve();

    x_ = multigridStep.getSol();
    /*
    Dune::VTKWriter<LeafView> writer(grid_.leafGridView());
    writer.addVertexData(x_,"solution");
    writer.write("stepresult");
    //std::cout<<"one LaplaceProblem sovled!"<<std::endl;
     */
}


