#include <config.h>
#include <sys/stat.h>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/onedgrid.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include "laplaceproblem.hh"
#include "mcsolver.hh"


#include <dune/fufem/functions/basisgridfunction.hh>

#include <dune/grid/io/file/dgfparser/dgfug.hh>
#include <dune/grid/io/file/dgfparser/dgfoned.hh>

//#include "interpolatebasis.hh"

#include <fstream>
#include <sstream>

#define DIMENSION 2
#define UGGRID
//#define ONEDGRID

const int nComponents = 1;
const int stochasticdim = 2;

using namespace Dune;

int main (int argc, char *argv[]) 
try
{

    // parse data file

    ParameterTree config;


    ParameterTreeParser::readINITree("mlmc.parset", config);
    ParameterTreeParser::readOptions(argc, argv, config);

    const int dim = DIMENSION;
    const int AD_REF = config.get<int>("AD_REF");



#ifdef UGGRID
    typedef Dune::UGGrid<dim> GridType;
#endif
#ifdef ONEDGRID
    typedef Dune::OneDGrid GridType;
#endif

    GridPtr<GridType> gridptr;


    if (dim == 1)
        gridptr = GridPtr<GridType>("grid_1d.dgf");

    if (dim == 2)
        gridptr= GridPtr<GridType>("grid_2d.dgf");

    GridType &grid = *gridptr;


    
    typedef double field_type;

    typedef Dune::FieldVector<field_type,dim> DomainType;
    typedef Dune::FieldVector<field_type,nComponents>  RangeType;
    typedef Dune::FieldVector<field_type,stochasticdim> SampleType;

    typedef Dune::BlockVector<Dune::FieldVector<field_type, nComponents> >   VectorType;

    //some hints:
    //Coefficient,RhsFunction,DirichletFunction are inherited from StochasticFunction
    //The array StochFunctions has three pointers of type StochasticFuction pointed to three instance of children types.
    //StochFunctionMap is an array with string as index.
    typedef std::map<std::string, StochasticFunction<DomainType, SampleType, RangeType>* > StochFunctionMap;
    StochFunctionMap stochFunctions;

    Coefficient<DomainType, SampleType, RangeType>           stochCoeff;
    RhsFunction<DomainType, SampleType, RangeType, dim>      stochRhs;
    DirichletFunction<DomainType, SampleType, RangeType>     stochDirNodes;

    stochFunctions["coefficient"]   = &stochCoeff;
    stochFunctions["rhs"]           = &stochRhs;
    stochFunctions["dirichletNodes"]= &stochDirNodes;

    //TODO
    MCSolver<GridType, VectorType, SampleType, DomainType, RangeType> mcsolver(grid, config, stochFunctions);
    
    mcsolver.MLMC_solve_uniform();

    VectorType sol = mcsolver.getSol();

    //Output result

    const int minRefine = config.get<int>("minRefine");
    VTKWriter<GridType::LevelGridView> writer(grid.levelGridView(minRefine));
    writer.addVertexData(sol,"solution");
    writer.write("result");
    


} catch (Exception e) {

    std::cout << e << std::endl;

}

