#ifndef DUNE_MLMC_SOLVERS_MONTECARLOSOLVER_HH
#define DUNE_MLMC_SOLVERS_MONTECARLOSOLVER_HH

// to incorporate stochastics
#include "jvrandom.h"
#include <fstream>
#include <dune/subgrid/subgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

template <class GridType, class VectorType, class SampleType, class DomainType, class RangeType>
class MCSolver
{
public:

    static const int dim = GridType::dimension;

    typedef typename DomainType::field_type field_type;

    typedef Dune::SubGrid<dim,GridType> SubGridType;

    typedef LaplaceProblem<SubGridType, VectorType, DomainType, RangeType> ProblemType;

    typedef typename ProblemType::BasisType LocalBasisType; //It's the basis of the SubGrid, which we are working on.

    typedef typename GridType::LeafGridView LeafView;

    typedef P1NodalBasis<LeafView,field_type> GlobalBasisType;//It's the basis of basic Grid.

    typedef std::map<std::string, StochasticFunction<DomainType, SampleType, RangeType>* > StochFunctionMap; 

    //Function are functions after parameters have been inserted into StochFunctions
    typedef Dune::VirtualFunction<DomainType, RangeType> Function;
    typedef SharedPointerMap<std::string, Function> FunctionMap;//map between a string and a function 

    MCSolver(GridType& grid, Dune::ParameterTree& parset, const StochFunctionMap& stochFunctions) :
        grid_(grid),
        lowerSubGrid_(grid),
		higherSubGrid_(grid),//use the grid to initially construct subGrid, the real construction remains to do later.
		minSubGrid_(grid),
        parset_(parset),
        stochFunctions_(stochFunctions)
    {
        stochDim_ = SampleType::dimension;
    }

    void MLMC_solve_uniform();

    VectorType getSol()
    {
        return solution_;
    }

private:
    VectorType   solution_;
    GridType& grid_;
    SubGridType lowerSubGrid_;
    SubGridType higherSubGrid_;
    SubGridType minSubGrid_;
    int stochDim_;
    Dune::ParameterTree& parset_;
    StochFunctionMap stochFunctions_;
    FunctionMap functions_;
};

#include "mcsolver-uniform.cc"

#endif // MONTECARLOSOLVER_HH
