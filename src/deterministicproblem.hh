#ifndef DUNE_MLMC_PROBLEM_CLASSES_DETERMINISTIC_PROBLEM_HH
#define DUNE_MLMC_PROBLEM_CLASSES_DETERMINISTIC_PROBLEM_HH

template <class VectorType>
class DeterministicProblem
{
    virtual void assemble() = 0;

    virtual void solve() = 0;

    virtual VectorType getSol() = 0;

};

#endif
