#ifndef DUNE_MLMC_COMMON_FUNCTIONS_HH
#define DUNE_MLMC_COMMON_FUNCTIONS_HH

#include <dune/common/function.hh>

template <class DomainType, class StochDomainType, class RangeType>
class StochasticFunction : public Dune::VirtualFunction<DomainType, RangeType>
//class StochasticFunction : public Dune::VirtualFunction<std::pair<DomainType,StochDomainType>, RangeType>
{
    public:
        //! Evaluate the function splitting up the arguments
        virtual void eval(const DomainType& x, const StochDomainType& omega, RangeType& y) const = 0;

        /*
        void evaluate(const std::pair<DomainType,StochDomainType>& x_om, RangeType& y) const
        {
            DomainType x = x_om.first;
            StochDomainType omega = x_om.second;
            eval(x, omega, y);
        }
        */
        //***Need to realize evalute, because it's a pure function in base class VirtualFunction***********
        void evaluate(const DomainType& x, RangeType& y) const
        //void evaluate(const std::pair<DomainType,StochDomainType>& x_om, RangeType& y) const
        {        	
        }
};

template <class DomainType, class StochDomainType, class RangeType>
class EvaluatedStochasticFunction : public Dune::VirtualFunction<DomainType, RangeType>
{
    typedef StochasticFunction<DomainType, StochDomainType, RangeType> StochasticFunctionType;
    public:
        EvaluatedStochasticFunction() {}

        EvaluatedStochasticFunction(StochasticFunctionType*& f, const StochDomainType& omega) :
            omega_(omega), f_(f)
        {}


        void evaluate(const DomainType& x, RangeType& y) const
        {
            f_->eval(x, omega_, y);

        }

    private:
        const StochDomainType& omega_;
        StochasticFunction<DomainType, StochDomainType, RangeType>* f_;

};

/*
// stochastic diffusion coefficient
template <class DomainType, class StochDomainType, class RangeType>
class Coefficient : public StochasticFunction<DomainType, StochDomainType, RangeType>
{
public:
    void eval(const DomainType& x, const StochDomainType& omega, RangeType& y) const
    {
        y = 1. + 0.1*std::cos(x.two_norm2())*omega[0] + 0.1*std::sin(x.two_norm2())*omega[1];
    }
};

// stochastic right hand side
template <class DomainType, class StochDomainType, class RangeType, int dim>
class RhsFunction : public StochasticFunction<DomainType, StochDomainType, RangeType>
{
public:
    void eval(const DomainType& x, const StochDomainType& omega, RangeType& y) const
    {
        double r = 0.7 + 0.1*(omega[0]+omega[1]);
        double K = 1. + 0.1*std::cos(x.two_norm2())*omega[0] + 0.1*std::sin(x.two_norm2())*omega[1];
        double dK = -0.1*std::sin(x.two_norm2())*omega[0] + 0.1*std::cos(x.two_norm2())*omega[1];
        double x_abs = x.two_norm();

        if (x_abs <= r)
        {
            y = 4*r*r*exp(2*omega[0]+2*omega[1])*(dim*K*(-1-r*r+x_abs*x_abs)+(-2-2*r*r+x_abs*x_abs)*x_abs*x_abs*dK);
        }
        else{
            y = -8*exp(2*omega[0]+2*omega[1])*(dim*K*((4-dim)*x_abs*x_abs-r*r)/2+(x_abs*x_abs-r*r)*x_abs*x_abs*dK);
        }

    }
};
// stochastic Dirichlet boundary function
template <class DomainType, class StochDomainType, class RangeType>
class DirichletFunction : public StochasticFunction<DomainType, StochDomainType, RangeType>
{
public:
    void eval(const DomainType& x, const StochDomainType& omega, RangeType& y) const
    {
        double r = 0.7 + 0.1*(omega[0]+omega[1]);
        y = pow(std::max((x.two_norm2() - r*r) * std::exp(omega[0]+omega[1]), 0.0) , 2.);
    }
};

*/
template <class DomainType, class StochDomainType, class RangeType>
class Coefficient : public StochasticFunction<DomainType, StochDomainType, RangeType>
{
public:
    void eval(const DomainType& x, const StochDomainType& omega, RangeType& y) const
    {
        y = 1.+omega[0];
    }
};

// stochastic right hand side
template <class DomainType, class StochDomainType, class RangeType, int dim>
class RhsFunction : public StochasticFunction<DomainType, StochDomainType, RangeType>
{
public:
    void eval(const DomainType& x, const StochDomainType& omega, RangeType& y) const
    {
        y = 1.+omega[0];

    }
};
// stochastic Dirichlet boundary function
template <class DomainType, class StochDomainType, class RangeType>
class DirichletFunction : public StochasticFunction<DomainType, StochDomainType, RangeType>
{
public:
    void eval(const DomainType& x, const StochDomainType& omega, RangeType& y) const
    {
        y = 0.+omega[0];
    }
};
#endif

